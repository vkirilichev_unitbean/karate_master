<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:hidden path="id"/>
<form:hidden path="senpaiPhotoId"/>

<div class="row">
    <div class="input-field col s12">
        <form:input path="senpaiName" id="senpaiName"/>
        <label for="senpaiName">Имя учителя</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <form:input path="senpaiAge" id="senpaiAge"/>
        <label for="senpaiAge">Возраст учителя</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12 ">
        <form:select path="senpaiBelt" id="senpaiBelt">
            <form:option value="NONE" label="Выберите пояс"/>
            <form:options items="${beltList}" />
        </form:select>
        <label for="senpaiBelt">Пояс учитея</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <form:input path="senpaiBiography" id="senpaiBiography"/>
        <label for="senpaiBiography">Описание</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <input type="file" name="photoSenpai">
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <img src="/pics/${senpai.senpaiPhotoId}" style="width: 100%" />
    </div>
</div>