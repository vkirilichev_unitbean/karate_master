package com.ub.karate_master.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;
import com.ub.karate_master.route.SenpaiAdminRoute;

public class SenpaiAddMenu extends BaseMenu {

    public SenpaiAddMenu(){
        this.name = "Добавить учителя";
        this.icon = MenuIcons.MDI_CONTENT_ADD;
        this.parent = new SenpaiMenu();
        this.url = SenpaiAdminRoute.ADD_SENPAI;
    }
}
