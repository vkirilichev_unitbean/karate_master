package com.ub.karate_master.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;

public class SenpaiMenu extends BaseMenu {

    public SenpaiMenu(){
        this.name = "Учитель";
        this.icon = MenuIcons.MDI_ACTION_INFO;
    }
}
