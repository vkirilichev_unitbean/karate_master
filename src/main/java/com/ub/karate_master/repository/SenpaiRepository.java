package com.ub.karate_master.repository;

import com.ub.karate_master.model.SenpaiDoc;
import com.ub.karate_master.resource.BeltResource;
import org.bson.types.ObjectId;

import java.util.List;

public interface SenpaiRepository {
    List<SenpaiDoc> getSenpaiList();
    void addSenpai(SenpaiDoc article);
    void updateSenpai (SenpaiDoc article);
    void deleteSenpai (ObjectId id);
    SenpaiDoc findSenpaiById(ObjectId id);
}
