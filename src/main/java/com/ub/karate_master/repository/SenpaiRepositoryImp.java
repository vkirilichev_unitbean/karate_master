package com.ub.karate_master.repository;

import com.ub.karate_master.model.Belt;
import com.ub.karate_master.model.SenpaiDoc;
import com.ub.karate_master.resource.BeltResource;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class SenpaiRepositoryImp implements SenpaiRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    private static final String COLLECTION_NAME = "senpaiDoc";

    @Override
    public List<SenpaiDoc> getSenpaiList() {

        return mongoTemplate.findAll(SenpaiDoc.class, COLLECTION_NAME);
    }

    @Override
    public void addSenpai(SenpaiDoc senpaiDoc) {
        mongoTemplate.insert(senpaiDoc, COLLECTION_NAME);
    }

    @Override
    public void updateSenpai(SenpaiDoc senpaiDoc) {
        mongoTemplate.save(senpaiDoc);
    }

    @Override
    public void deleteSenpai(ObjectId id) {
        SenpaiDoc articleDocument = this.findSenpaiById(id);
        mongoTemplate.remove(articleDocument);
    }

    @Override
    public SenpaiDoc findSenpaiById(ObjectId id) {
        return mongoTemplate.findById(id, SenpaiDoc.class);
    }



}
