package com.ub.karate_master.controller;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import com.ub.karate_master.model.Belt;
import com.ub.karate_master.model.SenpaiDoc;
import com.ub.karate_master.repository.SenpaiRepository;
import com.ub.karate_master.route.SenpaiAdminRoute;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class SenpaiAdminController {

    @Autowired
    private SenpaiRepository senpaiRepository;

    @Autowired
    private PictureRepository pictureRepository;

    @RequestMapping(value = SenpaiAdminRoute.ADD_SENPAI, method = RequestMethod.GET)
    public static String addSenpai(Model model){
        SenpaiDoc senpai = new SenpaiDoc();
        senpai.setId(new ObjectId());
        senpai.setSenpaiPhotoId(new ObjectId());
        model.addAttribute("senpai", senpai);
        return "com.ub.karate_master.senpai.admin.add";
    }

    @RequestMapping(value = SenpaiAdminRoute.ADD_SENPAI, method = RequestMethod.POST)
    public String add(@ModelAttribute SenpaiDoc senpaiDoc,
                      @RequestParam(required = false) MultipartFile photoSenpai, RedirectAttributes ra){

        PictureDoc pictureDoc = pictureRepository.save(senpaiDoc.getSenpaiPhotoId(), photoSenpai);
        senpaiDoc.setSenpaiPhotoId(pictureDoc.getId());
        senpaiRepository.addSenpai(senpaiDoc);
        ra.addAttribute("id", senpaiDoc.getId().toString());

        return RouteUtils.redirectTo(SenpaiAdminRoute.EDIT_SENPAI);
    }


    @ModelAttribute
    public Map getBeltList() throws  Exception{

        Map referenceBelt = new HashMap();
        Map<String, Belt> senpaiBelt = new LinkedHashMap<String, Belt>();
        senpaiBelt.put("Белый", Belt.WHITE);
        senpaiBelt.put("Оранжевый", Belt.ORANGE);
        senpaiBelt.put("Синий", Belt.BLUE);
        senpaiBelt.put("Желтый",Belt.YELLOW);
        senpaiBelt.put("Желтый", Belt.GREEN);
        senpaiBelt.put("Желтый", Belt.BROWN);
        senpaiBelt.put("Чергый", Belt.BLACK);
        referenceBelt.put("beltList", senpaiBelt);

        return referenceBelt;
    }
}
