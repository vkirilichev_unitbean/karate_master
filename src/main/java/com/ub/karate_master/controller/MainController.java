package com.ub.karate_master.controller;

import com.ub.karate_master.model.Belt;
import com.ub.karate_master.model.SenpaiDoc;
import com.ub.karate_master.repository.SenpaiRepository;
import com.ub.karate_master.resource.BeltResource;
import com.ub.karate_master.route.ClientRoute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private SenpaiRepository senpaiRepository;

    @RequestMapping(value = ClientRoute.ROOT, method = RequestMethod.GET)
    public String showIndexPage(Model model){

        model.addAttribute("senpaiList", senpaiRepository.getSenpaiList());
        model.addAttribute("beltColor", BeltResource.setBeltColor(senpaiRepository.getSenpaiList()));

        return "com.ub.karate_master.client.index";
    }
}
