package com.ub.karate_master.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SenpaiDoc {

    @Id
    private ObjectId id;
    private String senpaiName;
    private Byte senpaiAge;
    private Belt senpaiBelt;
    private String senpaiBiography;
    private ObjectId senpaiPhotoId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getSenpaiName() {
        return senpaiName;
    }

    public void setSenpaiName(String senpaiName) {
        this.senpaiName = senpaiName;
    }

    public Byte getSenpaiAge() {
        return senpaiAge;
    }

    public void setSenpaiAge(Byte senpaiAge) {
        this.senpaiAge = senpaiAge;
    }

    public Belt getSenpaiBelt() {
        return senpaiBelt;
    }

    public void setSenpaiBelt(Belt senpaiBelt) {
        this.senpaiBelt = senpaiBelt;
    }

    public String getSenpaiBiography() {
        return senpaiBiography;
    }

    public void setSenpaiBiography(String senpaiBiography) {
        this.senpaiBiography = senpaiBiography;
    }

    public ObjectId getSenpaiPhotoId() {
        return senpaiPhotoId;
    }

    public void setSenpaiPhotoId(ObjectId senpaiPhotoId) {
        this.senpaiPhotoId = senpaiPhotoId;
    }
}

