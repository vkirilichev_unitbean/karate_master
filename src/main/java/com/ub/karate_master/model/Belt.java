package com.ub.karate_master.model;

public enum Belt {

    WHITE,
    ORANGE,
    BLUE,
    YELLOW,
    GREEN,
    BROWN,
    BLACK,
}
