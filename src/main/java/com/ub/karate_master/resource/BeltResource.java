package com.ub.karate_master.resource;

import com.ub.karate_master.model.Belt;
import com.ub.karate_master.model.SenpaiDoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeltResource {

    public static String BlackBelt = "/static/karate_master/img/belts/black_belt.png";
    public static String GreenBelt = "/static/karate_master/img/belts/green_belt.png";


    public static String setBeltColor(List<SenpaiDoc> senpaiDocs){
        String beltColor = null;
        for (SenpaiDoc senpai: senpaiDocs){
            switch(senpai.getSenpaiBelt()){
                case BLACK:
                    beltColor = BeltResource.BlackBelt;
                    break;
                case GREEN:
                    beltColor = BeltResource.GreenBelt;
                    break;
            }
        }
        return  beltColor;
    }
}
