package com.ub.karate_master.route;

import com.ub.core.base.routes.BaseRoutes;

public class SenpaiAdminRoute {

    public static final String ROOT = BaseRoutes.ADMIN + "/senpai";

    public static final String ADD_SENPAI = ROOT + "/add-senpai";
    public static final String EDIT_SENPAI = ROOT + "/edit-senpai";
    public static final String ALL_SENPAI = ROOT + "/all-senpai";
    public static final String REMOVE_SENPAI = ROOT + "/remove-senpai";
}
