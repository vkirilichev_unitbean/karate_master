<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.ub.news_market_core.route.BlogAdminRoute" %>
<form action="<%=BlogAdminRoute.REMOVE%>" method="post">
    <input type="hidden" name="id" value="${id}">
    <h1>Внимание !</h1>
    <p>Документы будет удален.</p>
    <button class="btn red">Удалить</button>
</form>