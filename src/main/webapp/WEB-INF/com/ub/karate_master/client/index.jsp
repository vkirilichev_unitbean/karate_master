<%@ page import="com.ub.karate_master.resource.BeltResource" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Главная страница</title>

	<link rel="stylesheet" href="/static/karate_master/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/karate_master/css/main.css">
    <link rel="stylesheet" href="/static/karate_master/css/media.css"/>
	<link rel="stylesheet" href="/static/karate_master/css/fonts.css"/>
</head>
<body>

	<header>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-md">
                    <ul class="navbar-nav ml-auto menu">
                        <li class="nav-item">
                            <a class="nav-link" href="#">История каратэ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Тренировки</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Ученики</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Контакты</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="top_line"></div>
        </div>

    		<div class="container">
    			<div class="row">
                    <div class="col-md-2">
                        <div class="kyokushin_block">
                            <img src="/static/karate_master/img/kyokushinkai.svg" class="img-fluid" alt="logo">
                        </div>
                    </div>
    				<div class="col-md-8">
    					<div class="title_block">
    						<h1>Волгоградская федерация киокушинкай каратэ-до</h1>
    						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe adipisci necessitatibus labore vero accusamus sint!</p>
    						<button class="my_btn btn_md">find out more</button>
    					</div>
    				</div>
    			</div>
    		</div>
	</header>

    <section class="params_s">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-heart"></i>
                        <h3>Мы будем тренировать наши сердца и тела для достижения твёрдого и непоколебимого духа</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-comment"></i>
                        <h3>Мы будем следовать истинному смыслу воинского пути, чтобы наши чувства всегда были наготове </h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-desktop"></i>
                        <h3>С истинным рвением мы будем стремиться культивировать дух самосовершенства (самообладания)</h3>

                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-desktop"></i>
                        <h3>Мы будем соблюдать правила этикета, уважения старших и воздержания от насилия</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-desktop"></i>
                        <h3>Мы будем следовать нашим духовным убеждениям и никогда не забудем истинную добродетель скромности клянусь</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="param_block">
                        <i class="fa fa-desktop"></i>
                        <h3>Мы будем стремиться к мудрости и силе, не ведая других желаний клянусь</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 offset-md-4">
                    <div class="param_block">
                        <i class="fa fa-desktop"></i>
                        <h3>Всю нашу жизнь через карате мы будем стремиться выполнять истинное предназначение пути Киокушинкайкан клянусь</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="works_s">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-1.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-2.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-3.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-4.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-5.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-6.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-7.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 p0">
                    <div class="work_block">
                        <div class="image_wrap">
                            <div class="work_descr">
                                <h4>PROJECT NAME</h4>
                                <p>some description</p>
                            </div>
                        </div>
                        <img src="/static/karate_master/img/work-8.jpg" class="img-fluid" alt="workItem">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team_s">
        <div class="container">
            <div class="row">
                <c:forEach items="${senpaiList}" var="senpai">
                    <div class="col-md-3 col-sm-6">
                    <div class="team_item">
                        <img src='/pics/${senpai.senpaiPhotoId}' class="img-fluid" >
                        <img src='${beltColor}' class="img-fluid">
                        <h4>${senpai.senpaiName}</h4>
                        <span>founder ceo</span><br>
                        <p>${senpai.senpaiBiography}</p>
                        <div class="team_btns">
                            <button class="soc_btn_grey">
                                <i class="fa fa-facebook"></i>
                            </button>
                            <button class="soc_btn_grey">
                                <i class="fa fa-twitter"></i>
                            </button>
                            <button class="soc_btn_grey">
                                <i class="fa fa-linkedin-square"></i>
                            </button>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
        </div>
    </section>

    <section class="download">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="download_block">
                        <h5>Приглашаем Вас и Ваших детей на занятия</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                        <button class="my_btn btn_lg" data-toggle="modal" data-target="#dlModal">Подать заявку</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="main_footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="footer_item">
                            <h5>location</h5>
                            <p>Lorem ipsum dolor sit <br> amet, consectetur adipisicing.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="footer_item">
                            <h5>location</h5>
                            <button class="soc_btn_white">
                                <i class="fa fa-facebook"></i>
                            </button>
                            <button class="soc_btn_white">
                                <i class="fa fa-twitter"></i>
                            </button>
                            <button class="soc_btn_white">
                                <i class="fa fa-linkedin-square"></i>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-4 hidden-xs hidden-sm">
                        <div class="footer_item">
                            <h5>location</h5>
                            <p>Lorem ipsum dolor sit <br> amet, consectetur adipisicing.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright_block">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="copy_txt">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, numquam.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button class="call_btn" data-toggle="modal" data-target="#dlModal">
            <span class="glyphicon glyphicon-earphone"></span>
        </button>
    </footer>

</body>
</html>
